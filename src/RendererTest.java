import objectdata.*;
import objectops.Renderer;
import rasterdata.ColorRaster;
import rasterdata.ZBuffer;
import transforms.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Objects;
import java.util.function.BiConsumer;

public class RendererTest {
    private final JFrame frame;
    private final JPanel panel;
    private final ColorRaster img;

    public RendererTest(int width, int height) {
        frame = new JFrame();
        frame.setLayout(new BorderLayout());
        frame.setResizable(false);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        img = new ColorRaster(width, height, new Col(50, 50, 50));
        panel = new JPanel() {
            private static final long serialVersionUID = 1L;

            @Override
            public void paintComponent(Graphics g) {
                super.paintComponent(g);
                img.present(g);
            }
        };
        panel.setPreferredSize(new Dimension(width, height));

        frame.add(panel, BorderLayout.CENTER);
        frame.pack();
        frame.setVisible(true);

    }

    // rotation on x,y plane
    public double getAzimuthToOrigin(Vec3D position) {
        return getAzimuthToPosition(position, new Vec3D());
    }

    public double getAzimuthToPosition(Vec3D lookingFromPosition, Vec3D lookingToPosition) {
        Vec3D delta = lookingToPosition.sub(lookingFromPosition);
        double aTan = Math.atan(delta.getY() / delta.getX());
        return aTan + ((delta.getX() < 0) ? Math.PI : 2 * Math.PI);
        /*double alpha = delta.ignoreZ().normalized()
                .map(vNorm -> Math.acos(vNorm.dot(new Vec2D(1, 0))))
                .orElse(0.0);
        return ((alpha > 0) ? 2 * Math.PI : Math.PI) - alpha;*/
    }

    // "elevation" on z
    public double getZenithToOrigin(Vec3D position) {
        return getZenithToPosition(position, new Vec3D());
    }

    public double getZenithToPosition(Vec3D lookingFromPosition, Vec3D lookingToPosition) {
        Vec3D delta = lookingToPosition.sub(lookingFromPosition);
        return Math.asin(delta.getZ() / delta.length());
        /*double alpha = delta.normalized()
                .map(vNorm -> Math.acos(vNorm.dot(new Vec3D(0, 0, 1))))
                .orElse(Math.PI / 2);
        return Math.PI / 2 - alpha;*/
    }


    public void start() {
        ZBuffer zBuffer = new ZBuffer(img);
        Renderer renderer = new Renderer(zBuffer);
        Scene scene = new Scene();
        scene.addSolid(new Arrow(), new Mat4Scale(3).mul(new Mat4Transl(1, 1, 1)));
        scene.addSolid(new Axis(), new Mat4Scale(10));
        scene.addSolid(new Cube(), new Mat4Scale(2).mul(new Mat4Transl(2, 2, 2)));
        scene.addSolid(new Pyramid(), new Mat4Scale(1).mul(new Mat4Transl(-2, -2, 2)));
        scene.addSolid(new Wall(Vertex::getColor), new Mat4Scale(1).mul(new Mat4Transl(2, -2, 2)));

        final Point2D[] mouseLook = {null, null};
        final double[] mouseWheelMoved = {0};
        HashSet<Integer> pressedKeys = new HashSet<>();

        final boolean[] dragging = {false};
        panel.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                //onClick.accept(e);
            }

            @Override
            public void mousePressed(MouseEvent e) {
                mouseLook[0] = new Point2D(e.getX(), e.getY());
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                if (dragging[0]) {
                    dragging[0] = false;
                    //onDragEnd.accept(e);
                }
                mouseLook[0] = null;
                mouseLook[1] = null;
            }
        });

        panel.addMouseMotionListener(new MouseMotionAdapter() {
            @Override
            public void mouseDragged(MouseEvent e) {
                dragging[0] = true;
                mouseLook[1] = new Point2D(e.getX(), e.getY());
            }
        });

        panel.addMouseWheelListener(new MouseAdapter() {
            @Override
            public void mouseWheelMoved(MouseWheelEvent e) {
                mouseWheelMoved[0] += e.getPreciseWheelRotation();
            }
        });

        frame.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                pressedKeys.add(e.getKeyCode());
            }

            @Override
            public void keyReleased(KeyEvent e) {
                pressedKeys.remove(e.getKeyCode());
            }
        });


        Vec3D cameraPosition = new Vec3D(7, 9, 6);
        final Camera[] camera = {new Camera()
                .withPosition(cameraPosition)
                .withAzimuth(getAzimuthToOrigin(cameraPosition))
                .withZenith(getZenithToOrigin(cameraPosition))};

        BiConsumer<Integer, Runnable> runIfPressed = (keyCode, fn) -> {
            if (pressedKeys.contains(keyCode)) fn.run();
        };

        Runnable render = () -> {
            zBuffer.clear();

            double oneDegree = 0.0174533;

            // rotate camera with arrows
            runIfPressed.accept(KeyEvent.VK_UP, () -> camera[0] = camera[0].addZenith(oneDegree));
            runIfPressed.accept(KeyEvent.VK_DOWN, () -> camera[0] = camera[0].addZenith(-oneDegree));
            runIfPressed.accept(KeyEvent.VK_LEFT, () -> camera[0] = camera[0].addAzimuth(oneDegree));
            runIfPressed.accept(KeyEvent.VK_RIGHT, () -> camera[0] = camera[0].addAzimuth(-oneDegree));
            // rotate camera with mouse "joystick"
            if (Arrays.stream(mouseLook).allMatch(Objects::nonNull)) {
                double xvec = mouseLook[1].getX() - mouseLook[0].getX();
                double yvec = mouseLook[1].getY() - mouseLook[0].getY();
                xvec /= 800;
                yvec /= 800;

                camera[0] = camera[0].addAzimuth(oneDegree * -xvec);
                camera[0] = camera[0].addZenith(oneDegree * -yvec);
            }

            final double[] moveBy = {0.1};
            // zoom camera with mouse wheel
            if (mouseWheelMoved[0] != 0) {
                camera[0] = camera[0].move(camera[0].getViewVector().mul(moveBy[0] * 5 * -mouseWheelMoved[0]));
                mouseWheelMoved[0] = 0;
            }
            // move camera with WSAD
            runIfPressed.accept(KeyEvent.VK_SHIFT, () -> moveBy[0] *= 3);
            runIfPressed.accept(KeyEvent.VK_SPACE, () -> camera[0] = camera[0].move(new Vec3D(0, 0, moveBy[0])));
            runIfPressed.accept(KeyEvent.VK_CONTROL, () -> camera[0] = camera[0].move(new Vec3D(0, 0, -moveBy[0])));
            Vec2D viewVectorXYVec2D = camera[0].getViewVector().ignoreZ().normalized().get();
            Vec3D viewVectorXY = new Vec3D(viewVectorXYVec2D.getX(), viewVectorXYVec2D.getY(), 0);
            runIfPressed.accept(KeyEvent.VK_W, () -> camera[0] = camera[0].move(viewVectorXY.mul(new Vec3D(moveBy[0], moveBy[0], 0))));
            runIfPressed.accept(KeyEvent.VK_S, () -> camera[0] = camera[0].move(viewVectorXY.mul(new Vec3D(-moveBy[0], -moveBy[0], 0))));
            runIfPressed.accept(KeyEvent.VK_A, () -> camera[0] = camera[0].move(new Vec3D(-viewVectorXY.getY(), viewVectorXY.getX(), 0).mul(moveBy[0])));
            runIfPressed.accept(KeyEvent.VK_D, () -> camera[0] = camera[0].move(new Vec3D(viewVectorXY.getY(), -viewVectorXY.getX(), 0).mul(moveBy[0])));

            double k = zBuffer.getHeight() / (double) zBuffer.getWidth();
            Mat4 projectionMatPersp = new Mat4PerspRH(Math.PI / 2, k, 0.1, 200);
            renderer.drawScene(
                    scene,
                    camera[0].getViewMatrix(),
                    projectionMatPersp, false
            );
            img.present(panel.getGraphics());
        };

        boolean running3D = true;
        Thread renderThread = new Thread(() -> {
            while (true) {
                if (running3D)
                    render.run();
                try {
                    Thread.sleep(16);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        renderThread.start();
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> new RendererTest(800, 600).start());
    }

}
