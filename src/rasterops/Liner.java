package rasterops;

import linalg.Lerp;
import objectdata.Vertex;
import rasterdata.ZBuffer;
import transforms.Col;

import java.util.function.Function;

public class Liner {

    private final ZBuffer zBuffer;
    private final Lerp lerp = new Lerp();

    public Liner(ZBuffer zBuffer) {
        this.zBuffer = zBuffer;
    }

    public void draw(Vertex start, Vertex end, Function<Vertex, Col> shader) {
        int count = (int) Math.max(
                Math.abs(end.getPosition().getX() - start.getPosition().getX()),
                Math.abs(end.getPosition().getY() - start.getPosition().getY())
        );

        for (int i = 0; i < count + 1; i++) {
            double t = i / (double) count;
            Vertex vRes = lerp.compute(start, end, t);
            zBuffer.setPixel(
                    (int) Math.round(vRes.getPosition().getX()),
                    (int) Math.round(vRes.getPosition().getY()),
                    vRes.getPosition().getZ(),
                    shader.apply(vRes));
        }
    }
}
