package rasterops;

import rasterdata.Raster;
import transforms.Col;

public class TrianglerColor extends Triangler {

    private final Raster<Col> raster;

    public TrianglerColor(Raster<Col> raster) {
        this.raster = raster;
    }

    @Override
    protected void setPixel(int x, int y, double z, Col color) {
        raster.setPixel(x, y, color);
    }

    @Override
    protected int getWidth() {
        return raster.getWidth();
    }

    @Override
    protected int getHeight() {
        return raster.getHeight();
    }
}
