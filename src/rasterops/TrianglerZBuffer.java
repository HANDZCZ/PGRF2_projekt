package rasterops;

import rasterdata.ZBuffer;
import transforms.Col;

public class TrianglerZBuffer extends Triangler {

    private final ZBuffer zBuffer;

    public TrianglerZBuffer(ZBuffer zBuffer) {
        this.zBuffer = zBuffer;
    }

    @Override
    protected void setPixel(int x, int y, double z, Col color) {
        zBuffer.setPixel(x, y, z, color);
    }

    @Override
    protected int getWidth() {
        return zBuffer.getWidth();
    }

    @Override
    protected int getHeight() {
        return zBuffer.getHeight();
    }
}
