package rasterops;

import linalg.Lerp;
import objectdata.Vertex;
import transforms.Col;

import java.util.Comparator;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public abstract class Triangler {

    private final Lerp lerp = new Lerp();

    private void drawHalf(Vertex top, Vertex bot, Vertex other, Vertex t1Vert, Function<Vertex, Col> shader) {
        int yMin = (int) top.getPosition().getY();
        yMin = Integer.max(yMin, 0);
        int yMax = (int) bot.getPosition().getY();
        yMax = Integer.min(yMax, getHeight());
        IntStream.range(yMin, yMax).parallel().forEach(y -> {
            double t1 = (y - t1Vert.getPosition().getY()) / (other.getPosition().getY() - t1Vert.getPosition().getY());
            double t2 = (y - top.getPosition().getY()) / (bot.getPosition().getY() - top.getPosition().getY());
            Vertex v1i = lerp.compute(t1Vert, other, t1);
            Vertex v2i = lerp.compute(top, bot, t2);

            Vertex viMin;
            Vertex viMax;
            if (v1i.getPosition().getX() < v2i.getPosition().getX()) {
                viMin = v1i;
                viMax = v2i;
            } else {
                viMin = v2i;
                viMax = v1i;
            }

            double xViMin = viMin.getPosition().getX();
            xViMin = Math.max(xViMin, 0);
            double xViMax = viMax.getPosition().getX();
            xViMax = Math.min(xViMax, getWidth());
            double offset = xViMin - Math.floor(xViMin);
            IntStream.range((int) Math.floor(xViMin), (int) Math.floor(xViMax)).parallel().forEach(x -> {
                        double t = (x + offset - viMin.getPosition().getX()) / (viMax.getPosition().getX() - viMin.getPosition().getX());
                        Vertex vRes = lerp.compute(viMin, viMax, t);
                        setPixel(x, y, vRes.getPosition().getZ(), shader.apply(vRes));
                    }
            );
        });
    }

    private void drawFirstHalf(Vertex top, Vertex bot, Vertex other, Function<Vertex, Col> shader) {
        drawHalf(top, bot, other, top, shader);
    }

    private void drawSecondHalf(Vertex top, Vertex bot, Vertex other, Function<Vertex, Col> shader) {
        drawHalf(top, bot, other, bot, shader);
    }

    public void draw(Vertex v1, Vertex v2, Vertex v3, Function<Vertex, Col> shader) {
        List<Vertex> vSorted = List.of(v1, v2, v3)
                .stream()
                .sorted(Comparator.comparingDouble(value -> value.getPosition().getY()))
                .collect(Collectors.toList());

        // draw first half
        drawFirstHalf(vSorted.get(0), vSorted.get(1), vSorted.get(2), shader);

        // draw second half
        drawSecondHalf(vSorted.get(1), vSorted.get(2), vSorted.get(0), shader);
    }

    protected abstract void setPixel(int x, int y, double z, Col color);

    protected abstract int getWidth();

    protected abstract int getHeight();
}
