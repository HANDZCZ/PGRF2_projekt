import rasterdata.ColorRaster;
import rasterdata.Raster;
import rasterdata.ZBuffer;
import transforms.Col;

public class Main {
    public static void main(String[] args) {
        Raster<Col> colorRaster = new ColorRaster(10, 10, new Col(0x0));
        ZBuffer zBuffer = new ZBuffer(colorRaster);
        zBuffer.setPixel(0, 0, 0.9, new Col(0x05));
        System.out.println(colorRaster.getPixel(0, 0).get().getRGB());
        zBuffer.setPixel(0, 0, 0.5, new Col(0xFF));
        System.out.println(colorRaster.getPixel(0, 0).get().getRGB());
        zBuffer.setPixel(0, 0, 0.7, new Col(0xFE));
        System.out.println(colorRaster.getPixel(0, 0).get().getRGB());
    }
}