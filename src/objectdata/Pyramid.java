package objectdata;

import transforms.Col;
import transforms.Point3D;

import java.util.List;

public class Pyramid implements Solid {
    private final List<Vertex> vertices;
    private final List<Integer> indices;
    private final List<Part> parts;

    public Pyramid() {
        vertices = List.of(
                new Vertex(new Point3D(0, 0, 0), new Col(255, 0, 0)), // 0
                new Vertex(new Point3D(0, 1, 0), new Col(0, 255, 0)), // 1
                new Vertex(new Point3D(1, 0, 0), new Col(0, 0, 255)), // 2
                new Vertex(new Point3D(1, 1, 0), new Col(127, 127, 127)), // 3

                new Vertex(new Point3D(0.5, 0.5, 2), new Col(99, 50, 30)) // 4
        );
        indices = List.of(
                0, 2, 3, 1, // bottom
                4, 0, 2, 3, 1, 0  // hat
        );
        parts = List.of(
                new Part(Topology.TRIANGLE_FAN, 0, 4),
                new Part(Topology.TRIANGLE_FAN, 4, 6)
        );
    }

    @Override
    public List<Vertex> getVertices() {
        return vertices;
    }

    @Override
    public List<Integer> getIndices() {
        return indices;
    }

    @Override
    public List<Part> getParts() {
        return parts;
    }
}
