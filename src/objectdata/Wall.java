package objectdata;

import objectops.NormalsCalculator;
import transforms.Col;
import transforms.Point3D;

import java.util.List;
import java.util.function.Function;

public class Wall implements Solid {
    private final List<Vertex> vertices;
    private final List<Integer> indices;
    private final List<Part> parts;

    public Wall(Function<Vertex, Col> shader) {
        vertices = List.of(
                new Vertex(new Point3D(0, 0, 3), new Col(127, 0, 0), 0, 0), // 0
                new Vertex(new Point3D(0, 0, 0), new Col(127, 0, 0), 0, 1), // 1
                new Vertex(new Point3D(3, 0, 3), new Col(127, 0, 0), 1, 0), // 2
                new Vertex(new Point3D(3, 0, 0), new Col(127, 0, 0), 1, 1), // 3

                new Vertex(new Point3D(3, 0, 3), new Col(127, 0, 0), 1, 0), // 4
                new Vertex(new Point3D(3, 0, 0), new Col(127, 0, 0), 1, 1), // 5
                new Vertex(new Point3D(3, 1, 3), new Col(127, 0, 0), 0, 0), // 6
                new Vertex(new Point3D(3, 1, 0), new Col(127, 0, 0), 0, 1), // 7

                // top
                new Vertex(new Point3D(3, 0, 3), new Col(127, 0, 0), 0, 1), // 8
                new Vertex(new Point3D(3, 1, 3), new Col(127, 0, 0), 1, 1), // 9
                new Vertex(new Point3D(0, 0, 3), new Col(127, 0, 0), 0, 0), // 10
                new Vertex(new Point3D(0, 1, 3), new Col(127, 0, 0), 1, 0), // 11

                // bottom
                new Vertex(new Point3D(0, 0, 0), new Col(127, 0, 0), 1, 0), // 12
                new Vertex(new Point3D(0, 1, 0), new Col(127, 0, 0), 0, 0), // 13
                new Vertex(new Point3D(3, 0, 0), new Col(127, 0, 0), 1, 1), // 14
                new Vertex(new Point3D(3, 1, 0), new Col(127, 0, 0), 0, 1), // 15


                new Vertex(new Point3D(3, 1, 3), new Col(127, 0, 0), 0, 0), // 16
                new Vertex(new Point3D(3, 1, 0), new Col(127, 0, 0), 0, 1), // 17
                new Vertex(new Point3D(0, 1, 3), new Col(127, 0, 0), 1, 0), // 18
                new Vertex(new Point3D(0, 1, 0), new Col(127, 0, 0), 1, 1), // 19

                new Vertex(new Point3D(0, 1, 3), new Col(127, 0, 0), 1, 0), // 20
                new Vertex(new Point3D(0, 1, 0), new Col(127, 0, 0), 1, 1), // 21
                new Vertex(new Point3D(0, 0, 3), new Col(127, 0, 0), 0, 0), // 22
                new Vertex(new Point3D(0, 0, 0), new Col(127, 0, 0), 0, 1)  // 23
        );
        indices = List.of(
                12, 13, 15, 14, // bottom
                10, 11, 9, 8, // top
                0, 1, 2, 3, // sides
                4, 5, 6, 7,
                16, 17, 18, 19,
                20, 21, 22, 23
        );

        parts = List.of(
                new Part(Topology.TRIANGLE_FAN, 0, 4, shader),
                new Part(Topology.TRIANGLE_FAN, 4, 4, shader),
                new Part(Topology.TRIANGLE_STRIP, 8, 4, shader),
                new Part(Topology.TRIANGLE_STRIP, 12, 4, shader),
                new Part(Topology.TRIANGLE_STRIP, 16, 4, shader),
                new Part(Topology.TRIANGLE_STRIP, 20, 4, shader)
        );

        NormalsCalculator.calculateNormals(vertices, indices, parts.get(0), true);
        NormalsCalculator.calculateNormals(vertices, indices, parts.get(1), false);
        NormalsCalculator.calculateNormals(vertices, indices, parts.get(2), false);
        NormalsCalculator.calculateNormals(vertices, indices, parts.get(3), false);
        NormalsCalculator.calculateNormals(vertices, indices, parts.get(4), false);
        NormalsCalculator.calculateNormals(vertices, indices, parts.get(5), false);
    }

    @Override
    public List<Vertex> getVertices() {
        return vertices;
    }

    @Override
    public List<Integer> getIndices() {
        return indices;
    }

    @Override
    public List<Part> getParts() {
        return parts;
    }
}
