package objectdata;

import objectops.NormalsCalculator;
import transforms.Bicubic;
import transforms.Col;
import transforms.Mat4;
import transforms.Point3D;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.IntStream;

public class Surface implements Solid {
    private final List<Vertex> vertices;
    private final List<Integer> indices;
    private final List<Part> parts;

    public Surface(Mat4 cubicMat, Point3D[] points, int numSecs, Function<Vertex, Col> shader) {
        this(cubicMat, points, numSecs);

        IntStream.range(0, parts.size()).parallel().forEach(i -> {
            Part part = parts.get(i);
            part.setShader(Optional.of(shader));
            NormalsCalculator.calculateNormals(vertices, indices, part, false);
        });
    }

    public Surface(Mat4 cubicMat, Point3D[] points, int numSecs) {
        double step = 1.0 / numSecs;
        int numPointsRowCol = numSecs + 1;

        // Create a bicubic surface using the base matrix and the control points
        Bicubic bicubic = new Bicubic(cubicMat, points);

        vertices = new ArrayList<>(numPointsRowCol * numPointsRowCol);
        // Iterate over the u and v parameters from 0 to 1
        for (double u = 0; 1 - u > -0.00001; u += step) {
            for (double v = 0; 1 - v > -0.00001; v += step) {
                // Compute the coordinates of a point on the bicubic surface
                // clamp for uv mapping
                u = Math.min(u, 1);
                v = Math.min(v, 1);
                Point3D point = bicubic.compute(u, v);
                Col color = new Col(Color.HSBtoRGB(
                        (float) (point.getX() * 0.20 + point.getY() * 0.15  + point.getZ() * 0.25),
                        1f,
                        1f
                ));
                vertices.add(new Vertex(point, color, u, v));
            }
        }

        this.indices = new ArrayList<>();
        this.parts = new ArrayList<>();
        int lastPartEnd = 0;

        // go through all squares
        for (int u = 1; u < numPointsRowCol; u++) {
            // create triangles for square
            for (int v = 0; v < numPointsRowCol - 1; v++) {
                // center
                indices.add(u * numPointsRowCol + v);
                indices.add((u - 1) * numPointsRowCol + v);
                indices.add((u - 1) * numPointsRowCol + v + 1);
                indices.add(u * numPointsRowCol + v + 1);
                parts.add(new Part(Topology.TRIANGLE_FAN, lastPartEnd, 4));
                lastPartEnd += 4;
            }
        }
    }


    @Override
    public List<Vertex> getVertices() {
        return vertices;
    }

    @Override
    public List<Integer> getIndices() {
        return indices;
    }

    @Override
    public List<Part> getParts() {
        return parts;
    }
}

