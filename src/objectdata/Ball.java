package objectdata;

import transforms.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Ball implements Solid {
    private final List<Vertex> vertices;
    private final List<Integer> indices;
    private final List<Part> parts;

    private final double deg45 = Math.PI / 2;
    private final Point3D[] controlPoints = new Point3D[]{
            new Point3D(0, 1, 0),
            new Point3D(0, 1, -0.5),
            new Point3D(0, 0.5, -1),
            new Point3D(0, 0, -1),


            new Point3D(0, 1, 0).mul(new Mat4Scale(1, 1.15, 1)).mul(new Mat4RotZ(deg45 / 3)),
            new Point3D(0, 1, -0.5).mul(new Mat4Scale(1, 1.15, 1)).mul(new Mat4RotZ(deg45 / 3)),
            new Point3D(0, 0.5, -1).mul(new Mat4Scale(1, 1.15, 1)).mul(new Mat4RotZ(deg45 / 3)),
            new Point3D(0, 0, -1),


            new Point3D(0, 1, 0).mul(new Mat4Scale(1, 1.15, 1)).mul(new Mat4RotZ(2 * deg45 / 3)),
            new Point3D(0, 1, -0.5).mul(new Mat4Scale(1, 1.15, 1)).mul(new Mat4RotZ(2 * deg45 / 3)),
            new Point3D(0, 0.5, -1).mul(new Mat4Scale(1, 1.15, 1)).mul(new Mat4RotZ(2 * deg45 / 3)),
            new Point3D(0, 0, -1),


            new Point3D(0, 1, 0).mul(new Mat4RotZ(deg45)),
            new Point3D(0, 1, -0.5).mul(new Mat4RotZ(deg45)),
            new Point3D(0, 0.5, -1).mul(new Mat4RotZ(deg45)),
            new Point3D(0, 0, -1),
    };

    private Point3D[] mulPoints(Mat4 rotationMat) {
        Point3D[] points = new Point3D[16];
        for (int i = 0; i < points.length; i++) {
            points[i] = controlPoints[i].mul(rotationMat);
        }
        return points;
    }

    public Ball(int numSecs) {
        final List<Surface> surfaces = new ArrayList<>(8);

        // bottom half
        for (int i = 0; i < 4; i++) {
            surfaces.add(new Surface(Cubic.BEZIER, mulPoints(new Mat4RotZ(i * deg45)), numSecs));
        }

        // top half
        Mat4 flipAndMove = new Mat4RotX(2 * deg45);
        for (int i = 0; i < 4; i++) {
            surfaces.add(new Surface(Cubic.BEZIER, mulPoints(flipAndMove.mul(new Mat4RotZ(i * deg45))), numSecs));
        }

        vertices = surfaces.stream().flatMap(surface -> surface.getVertices().stream()).collect(Collectors.toList());
        indices = new ArrayList<>(surfaces.get(0).getIndices().size() * 8);
        indices.addAll(surfaces.get(0).getIndices());
        int indicesOffset = surfaces.get(0).getVertices().size();
        for (int i = 1; i < surfaces.size(); i++) {
            Surface surface = surfaces.get(i);
            int finalIndicesOffset = indicesOffset;
            surface.getIndices().stream().mapToInt(value -> value + finalIndicesOffset).forEach(indices::add);
            indicesOffset += surface.getVertices().size();
        }

        List<Part> fistSurfParts = surfaces.get(0).getParts();
        Part fistSurfLastPart = fistSurfParts.get(fistSurfParts.size() - 1);
        int lastUnusedIndex = fistSurfLastPart.getOffset() + fistSurfLastPart.getCount();
        for (int i = 1; i < surfaces.size(); i++) {
            for (Part p : surfaces.get(i).getParts()) {
                p.setOffset(lastUnusedIndex);
                lastUnusedIndex += p.getCount();
            }
        }

        parts = surfaces.stream().flatMap(surface -> surface.getParts().stream()).collect(Collectors.toList());
    }

    @Override
    public List<Vertex> getVertices() {
        return vertices;
    }

    @Override
    public List<Integer> getIndices() {
        return indices;
    }

    @Override
    public List<Part> getParts() {
        return parts;
    }
}
