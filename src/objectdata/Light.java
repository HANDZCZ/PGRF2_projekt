package objectdata;

import transforms.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Light implements Solid {
    private final List<Vertex> vertices;
    private final List<Integer> indices;
    private final List<Part> parts;

    private final Col color;
    private Vec3D position;
    private final double power;


    public Light(Col color, Vec3D position, boolean visible, double power) {
        this(color, position, power, visible, 0.05);
    }

    public Light(Col color, Vec3D position, double power, boolean visible, double scale) {
        this.color = color;
        this.position = position;
        this.power = power;

        if (visible) {
            Mat4 translateAndScale = new Mat4Scale(scale).mul(new Mat4Transl(this.position.add(new Vec3D(-0.5 * scale, -0.5 * scale, -0.5 * scale))));
            Cube c = new Cube();
            vertices = c.getVertices().stream()
                    .map(vertex -> vertex.withMatToPosition(translateAndScale).withColor(this.color))
                    .collect(Collectors.toList());
            indices = c.getIndices();
            parts = c.getParts();
        } else {
            vertices = new ArrayList<>();
            indices = new ArrayList<>();
            parts = new ArrayList<>();
        }
    }

    @Override
    public List<Vertex> getVertices() {
        return vertices;
    }

    @Override
    public List<Integer> getIndices() {
        return indices;
    }

    @Override
    public List<Part> getParts() {
        return parts;
    }

    public Col getColor() {
        return color;
    }

    public Vec3D getPosition() {
        return position;
    }

    public double getPower() {
        return power;
    }

    public void translate(Vec3D by) {
        this.position = position.add(by);
    }
}
