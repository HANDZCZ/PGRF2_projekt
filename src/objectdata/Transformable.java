package objectdata;

import transforms.Mat4;

public interface Transformable<T> {
    T transform(Mat4 transMat, Mat4 modelMat);
    T dehomog();
    T toViewport(int width, int height);

}
