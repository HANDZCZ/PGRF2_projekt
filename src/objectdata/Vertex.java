package objectdata;

import linalg.Vectorizable;
import transforms.Col;
import transforms.Mat4;
import transforms.Point3D;
import transforms.Vec3D;

public class Vertex implements Vectorizable<Vertex>, Transformable<Vertex> {
    private final Point3D position;
    private final Point3D modelPosition;
    private final Col color;
    private double one = 1;
    private double u, v;
    private Vec3D normal = new Vec3D();

    public Vertex(Point3D position, Col color) {
        this.position = position;
        this.modelPosition = new Point3D(position);
        this.color = color;
    }

    private Vertex(Point3D position, Col color, double one, double u, double v, Vec3D normal, Point3D modelPosition) {
        this.position = position;
        this.color = color;
        this.u = u;
        this.v = v;
        this.one = one;
        this.normal = normal;
        this.modelPosition = modelPosition;
    }

    public Vertex(Point3D position, Col color, double u, double v) {
        this(position, color);
        this.u = u;
        this.v = v;
    }

    public Point3D getPosition() {
        return position;
    }

    public Point3D getModelPosition() {
        return modelPosition;
    }

    public Vec3D getNormal() {
        return normal.mul(1 / one);
    }

    public Col getColor() {
        return color.mul(1 / one);
    }

    public double getU() {
        return u / one;
    }

    public double getV() {
        return v / one;
    }

    @Override
    public Vertex mul(double t) {
        return new Vertex(position.mul(t), color.mul(t), one * t, u * t, v * t, normal.mul(t), modelPosition.mul(t));
    }

    @Override
    public Vertex add(Vertex other) {
        return new Vertex(position.add(other.getPosition()), color.add(other.color), one + other.one, u + other.u, v + other.v, normal.add(other.normal), modelPosition.add(other.getModelPosition()));
    }

    @Override
    public String toString() {
        return "{" + position + "; " + color + "}";
    }

    @Override
    public Vertex transform(Mat4 transMat, Mat4 modelMat) {
        return new Vertex(position.mul(transMat), color, one, u, v, new Vec3D(new Point3D(normal).withW(0).mul(modelMat)).normalized().orElse(new Vec3D()), modelPosition.mul(modelMat));
    }

    @Override
    public Vertex dehomog() {
        double w = position.getW();
        return new Vertex(new Point3D(position.dehomog().orElse(new Vec3D())), color.mul(1 / w), one / w, u / w, v / w, normal.normalized().orElse(new Vec3D()).mul(1 / w), modelPosition);
    }

    @Override
    public Vertex toViewport(int width, int height) {
        return new Vertex(new Point3D(
                Math.round((position.getX() + 1) / 2 * (width - 1)),
                Math.round((1 - (position.getY() + 1) / 2) * (height - 1)),
                position.getZ()
        ), color, one, u, v, normal, modelPosition);
    }

    public void addNormal(Vec3D normal) {
        this.normal = this.normal.add(normal);
    }

    public Vertex withMatToPosition(Mat4 mat) {
        return new Vertex(position.mul(mat), color, one, u, v, normal, modelPosition.mul(mat));
    }

    public Vertex withColor(Col color) {
        return new Vertex(position, color, one, u, v, normal, modelPosition);
    }
}
