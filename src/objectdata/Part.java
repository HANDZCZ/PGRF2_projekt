package objectdata;

import transforms.Col;

import java.util.Optional;
import java.util.function.Function;

public class Part {
    private final Topology topology;
    private final int count;
    private int offset;
    private Optional<Function<Vertex, Col>> shader = Optional.empty();

    public Part(Topology topology, int offset, int count) {
        this.topology = topology;
        this.offset = offset;
        this.count = count;
    }

    public Part(Topology topology, int offset, int count, Function<Vertex, Col> shader) {
        this(topology, offset, count);
        this.shader = Optional.of(shader);
    }

    public Topology getTopology() {
        return topology;
    }

    public int getOffset() {
        return offset;
    }

    public int getCount() {
        return count;
    }

    public Optional<Function<Vertex, Col>> getShader() {
        return shader;
    }

    public void setShader(Optional<Function<Vertex, Col>> shader) {
        this.shader = shader;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }
}
