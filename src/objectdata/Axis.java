package objectdata;

import transforms.Col;
import transforms.Point3D;

import java.util.List;

public class Axis implements Solid {
    private final List<Vertex> vertices;
    private final List<Integer> indices;
    private final List<Part> parts;

    public Axis() {
        vertices = List.of(
                new Vertex(new Point3D(0, 0, 0), new Col(255, 0, 0)), // 0
                new Vertex(new Point3D(0, 0, 0), new Col(0, 255, 0)), // 1
                new Vertex(new Point3D(0, 0, 0), new Col(0, 0, 255)), // 2

                new Vertex(new Point3D(1, 0, 0), new Col(255, 0, 0)), // 3
                new Vertex(new Point3D(0, 1, 0), new Col(0, 255, 0)), // 4
                new Vertex(new Point3D(0, 0, 1), new Col(0, 0, 255))  // 5
        );
        indices = List.of(
                0, 3, // X
                1, 4, // Y
                2, 5  // Z

        );
        parts = List.of(
                new Part(Topology.LINE_LIST, 0, 2),
                new Part(Topology.LINE_LIST, 2, 2),
                new Part(Topology.LINE_LIST, 4, 2)
        );
    }

    @Override
    public List<Vertex> getVertices() {
        return vertices;
    }

    @Override
    public List<Integer> getIndices() {
        return indices;
    }

    @Override
    public List<Part> getParts() {
        return parts;
    }
}
