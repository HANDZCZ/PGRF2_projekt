import objectdata.*;
import objectops.ShaderUtils;
import rasterdata.ColorRaster;
import rasterdata.ZBuffer;
import transforms.*;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.time.Instant;
import java.util.*;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Renderer {
    private final JFrame frame;
    private final JPanel panel;
    private final ColorRaster img;
    private final List<BufferedImage> textures;

    public Renderer(int width, int height) {
        textures = new ArrayList<>();
        try {
            BufferedImage image = ImageIO.read(new File("./textures/brick.jpg"));
            textures.add(image);
            image = ImageIO.read(new File("./textures/moss.jpg"));
            textures.add(image);
        } catch (IOException e) {
            e.printStackTrace();
        }

        frame = new JFrame();
        frame.setLayout(new BorderLayout());
        frame.setResizable(false);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        img = new ColorRaster(width, height, new Col(50, 50, 50));
        panel = new JPanel() {
            private static final long serialVersionUID = 1L;

            @Override
            public void paintComponent(Graphics g) {
                super.paintComponent(g);
                img.present(g);
            }
        };
        panel.setPreferredSize(new Dimension(width, height));

        frame.add(panel, BorderLayout.CENTER);
        frame.pack();
        frame.setVisible(true);
    }

    private Point3D[] createAnimatedSurface(Point3D[] startSurface, Point3D[] endSurface, double t) {
        // Create an array of 16 interpolated control points
        Point3D[] controlPoints = new Point3D[16];
        for (int i = 0; i < 16; i++) {
            controlPoints[i] = startSurface[i].mul(1 - t).add(endSurface[i].mul(t));
        }

        return controlPoints;
    }


    // rotation on x,y plane
    public double getAzimuthToOrigin(Vec3D position) {
        return getAzimuthToPosition(position, new Vec3D());
    }

    private double getAzimuthToPosition(Vec3D lookingFromPosition, Vec3D lookingToPosition) {
        Vec3D delta = lookingToPosition.sub(lookingFromPosition);
        double aTan = Math.atan(delta.getY() / delta.getX());
        return aTan + ((delta.getX() < 0) ? Math.PI : 2 * Math.PI);
        /*double alpha = delta.ignoreZ().normalized()
                .map(vNorm -> Math.acos(vNorm.dot(new Vec2D(1, 0))))
                .orElse(0.0);
        return ((alpha > 0) ? 2 * Math.PI : Math.PI) - alpha;*/
    }

    // "elevation" on z
    public double getZenithToOrigin(Vec3D position) {
        return getZenithToPosition(position, new Vec3D());
    }

    private double getZenithToPosition(Vec3D lookingFromPosition, Vec3D lookingToPosition) {
        Vec3D delta = lookingToPosition.sub(lookingFromPosition);
        return Math.asin(delta.getZ() / delta.length());
        /*double alpha = delta.normalized()
                .map(vNorm -> Math.acos(vNorm.dot(new Vec3D(0, 0, 1))))
                .orElse(Math.PI / 2);
        return Math.PI / 2 - alpha;*/
    }

    private void run() {
        final Point2D[] mouseLook = {null, null};
        final double[] mouseWheelMoved = {0};
        HashSet<Integer> pressedKeys = new HashSet<>();

        final boolean[] dragging = {false};
        panel.addMouseMotionListener(new MouseMotionAdapter() {
            @Override
            public void mouseDragged(MouseEvent e) {
                dragging[0] = true;
                mouseLook[1] = new Point2D(e.getX(), e.getY());
            }
        });

        panel.addMouseWheelListener(new MouseAdapter() {
            @Override
            public void mouseWheelMoved(MouseWheelEvent e) {
                mouseWheelMoved[0] += e.getPreciseWheelRotation();
            }
        });

        panel.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
            }

            @Override
            public void mousePressed(MouseEvent e) {
                mouseLook[0] = new Point2D(e.getX(), e.getY());
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                if (dragging[0]) {
                    dragging[0] = false;
                }
                mouseLook[0] = null;
                mouseLook[1] = null;
            }
        });

        Vec3D cameraPosition = new Vec3D(10, -2, 8);
        Vec3D lookingAt = new Vec3D(3, 3, 2);
        final Camera[] camera = {new Camera()
                .withPosition(cameraPosition)
                .withAzimuth(getAzimuthToPosition(cameraPosition, lookingAt))
                .withZenith(getZenithToPosition(cameraPosition, lookingAt))};

        ZBuffer zBuffer = new ZBuffer(img);
        objectops.Renderer renderer = new objectops.Renderer(zBuffer);
        Scene scene = new Scene();
        scene.addSolid(new Axis(), new Mat4Scale(10));
        scene.addSolid(new Arrow(), new Mat4Scale(3).mul(new Mat4Transl(1, 1, 1)));
        scene.addSolid(new Cube(), new Mat4Scale(2).mul(new Mat4Transl(2, 2, 2)));
        scene.addSolid(new Pyramid(), new Mat4Scale(1).mul(new Mat4Transl(-2, -2, 2)));
        Light wallLight = new Light(new Col(255, 255, 255), new Vec3D(6, -2, 2.5), true, 1);
        scene.addSolid(wallLight);
        scene.addSolid(new Wall(ShaderUtils.phongShading(
                wallLight,
                0.15,
                1,
                15,
                vertex -> new Col(textures.get(0).getRGB(
                        (int) ((textures.get(0).getWidth() - 1) * vertex.getU()),
                        (int) ((textures.get(0).getHeight() - 1) * vertex.getV())
                )),
                () -> camera[0])),
                new Mat4(
                        new Point3D(0, 1.0, 0, 0),
                        new Point3D(-1.0, 0, 0, 0),
                        new Point3D(0, 0, 1.0, 0),
                        new Point3D(5.3, -3.8, 1.7, 1.0)
                )
        );

        final boolean[] useOrth = {false};
        final boolean[] wireframe = {false};
        final int[] selectedSolid = {1};
        final boolean[] highlight = {true};
        List<Optional<Function<Vertex, Col>>> originalShaders = new ArrayList<>();
        Runnable removeHighlighter = () -> {
            List<Part> parts = scene.getSolids().get(selectedSolid[0]).getParts();
            for (int i = 0; i < originalShaders.size(); i++) {
                parts.get(i).setShader(originalShaders.get(i));
            }
            originalShaders.clear();
        };
        Runnable highlighter = () -> {
            removeHighlighter.run();
            if (!highlight[0]) return;

            List<Part> parts = scene.getSolids().get(selectedSolid[0]).getParts();
            for (Part part : parts) {
                Optional<Function<Vertex, Col>> originalShader = part.getShader();
                originalShaders.add(originalShader);
                part.setShader(Optional.of(vertex -> new Col(123, 45, 67)));
            }
        };

        frame.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                pressedKeys.add(e.getKeyCode());
            }

            @Override
            public void keyReleased(KeyEvent e) {
                pressedKeys.remove(e.getKeyCode());
                BiConsumer<Integer, Runnable> runIfReleased = (keyCode, fn) -> {
                    if (e.getKeyCode() == keyCode) fn.run();
                };

                runIfReleased.accept(KeyEvent.VK_N, () -> useOrth[0] = !useOrth[0]);
                runIfReleased.accept(KeyEvent.VK_V, () -> wireframe[0] = !wireframe[0]);
                runIfReleased.accept(KeyEvent.VK_C, () -> {
                    removeHighlighter.run();
                    highlight[0] = !highlight[0];
                });
                runIfReleased.accept(KeyEvent.VK_Q, () -> {
                    removeHighlighter.run();
                    selectedSolid[0] = (selectedSolid[0] % (scene.getSolids().size() - 1)) + 1;
                });
            }
        });

        // surface
        Point3D[] controlPoints = new Point3D[]{
                new Point3D(-1, -1, 2),
                new Point3D(-0.5, -1, -1),
                new Point3D(0.5, -1, -2),
                new Point3D(1, -1, 1),
                new Point3D(-1, -0.5, -1),
                new Point3D(-0.5, -0.5, 2),
                new Point3D(0.5, -0.5, -1),
                new Point3D(1, -0.5, -2),
                new Point3D(-1, 0.5, 2),
                new Point3D(-0.5, 0.5, -2),
                new Point3D(0.5, 0.5, 1),
                new Point3D(1, 0.5, -1),
                new Point3D(-1, 1, -2),
                new Point3D(-0.5, 1, 1),
                new Point3D(0.5, 1, 2),
                new Point3D(1, 1, -1)
        };
        scene.addSolid(new Surface(Cubic.COONS, controlPoints, 10), new Mat4RotZ(Math.PI).mul(new Mat4Transl(0, 3, 1)).mul(new Mat4Scale(3)));
        scene.addSolid(new Ball(10), new Mat4Scale(2).mul(new Mat4Transl(6, 6, 0)));

        Light surfaceLight = new Light(new Col(255, 255, 255), new Vec3D(6.5, 9, 4), true, 2);
        scene.addSolid(surfaceLight);
        scene.addSolid(new Surface(Cubic.COONS, controlPoints, 10, ShaderUtils.phongShading(
                surfaceLight,
                0.15,
                0.1,
                5,
                vertex -> new Col(textures.get(1).getRGB(
                        (int) ((textures.get(1).getWidth() - 1) * vertex.getU()),
                        (int) ((textures.get(1).getHeight() - 1) * vertex.getV())
                )),
                () -> camera[0])
        ), new Mat4RotZ(Math.PI).mul(new Mat4Transl(2, 3, 1)).mul(new Mat4Scale(3)));

        BiConsumer<Integer, Runnable> runIfPressed = (keyCode, fn) -> {
            if (pressedKeys.contains(keyCode)) fn.run();
        };

        final double[] t = {0};

        Runnable render = () -> {
            zBuffer.clear();

            double oneDegree = 0.0174533;

            // pyramid
            {
                Mat4 pyramidMat = scene.getModelMats().get(3);
                pyramidMat = (new Mat4Transl(-0.5, -0.5, 0).mul(new Mat4RotZ(oneDegree * 3)).mul(new Mat4Transl(0.5, 0.5, 0))).mul(pyramidMat);
                scene.getModelMats().set(3, pyramidMat);
            }

            // surface
            {
                Point3D[] endSurface = new Point3D[controlPoints.length];
                for (int i = 0; i < endSurface.length; i++) {
                    Point3D start = controlPoints[i];
                    endSurface[i] = new Point3D(start.getX(), start.getY(), -start.getZ());
                }

                Point3D[] animatedControlPoints = createAnimatedSurface(controlPoints, endSurface, ((int) t[0] % 2 == 0) ? t[0] % 1 : 1 - t[0] % 1);
                scene.getSolids().set(6, new Surface(Cubic.COONS, animatedControlPoints, 20));
            }
            t[0] += 0.002;


            // selected solid transform
            // leave out XYZ axis
            if (selectedSolid[0] != 0) {
                highlighter.run();

                Mat4 selectedSolidMat = scene.getModelMats().get(selectedSolid[0]);
                List<Point3D> selectedSolidPoints = scene.getSolids().get(selectedSolid[0]).getVertices().parallelStream().map(vertex -> vertex.getPosition().mul(selectedSolidMat)).collect(Collectors.toList());
                Vec3D minXYZ = new Vec3D(selectedSolidPoints.get(0));
                Vec3D maxXYZ = new Vec3D(selectedSolidPoints.get(0));
                for (int i = 1; i < selectedSolidPoints.size(); i++) {
                    Point3D current = selectedSolidPoints.get(i);
                    if (minXYZ.getX() > current.getX()) minXYZ = minXYZ.withX(current.getX());
                    if (minXYZ.getY() > current.getY()) minXYZ = minXYZ.withY(current.getY());
                    if (minXYZ.getZ() > current.getZ()) minXYZ = minXYZ.withZ(current.getZ());

                    if (maxXYZ.getX() < current.getX()) maxXYZ = maxXYZ.withX(current.getX());
                    if (maxXYZ.getY() < current.getY()) maxXYZ = maxXYZ.withY(current.getY());
                    if (maxXYZ.getZ() < current.getZ()) maxXYZ = maxXYZ.withZ(current.getZ());
                }
                final Mat4 moveToCenterMat = new Mat4Transl(minXYZ.add(maxXYZ.sub(minXYZ).mul(0.5)).opposite());
                final Mat4[] resultingMat = {selectedSolidMat.mul(moveToCenterMat)};

                // scale
                runIfPressed.accept(KeyEvent.VK_X, () -> resultingMat[0] = resultingMat[0].mul(new Mat4Scale(1.01)));
                runIfPressed.accept(KeyEvent.VK_Y, () -> resultingMat[0] = resultingMat[0].mul(new Mat4Scale(0.99)));


                // rotation
                int[] direction = new int[]{1};
                runIfPressed.accept(KeyEvent.VK_SHIFT, () -> direction[0] = -1);
                runIfPressed.accept(KeyEvent.VK_R, () -> resultingMat[0] = resultingMat[0].mul(new Mat4RotX(oneDegree * 5 * direction[0])));
                runIfPressed.accept(KeyEvent.VK_T, () -> resultingMat[0] = resultingMat[0].mul(new Mat4RotY(oneDegree * 5 * direction[0])));
                runIfPressed.accept(KeyEvent.VK_Z, () -> resultingMat[0] = resultingMat[0].mul(new Mat4RotZ(oneDegree * 5 * direction[0])));
                moveToCenterMat.inverse().ifPresent(mat -> resultingMat[0] = resultingMat[0].mul(mat));

                // translation
                final Vec3D[] translation = {new Vec3D()};
                final double[] moveBy = {0.1};
                runIfPressed.accept(KeyEvent.VK_I, () -> translation[0] = translation[0].add(new Vec3D(-moveBy[0], 0, 0)));
                runIfPressed.accept(KeyEvent.VK_K, () -> translation[0] = translation[0].add(new Vec3D(moveBy[0], 0, 0)));
                runIfPressed.accept(KeyEvent.VK_J, () -> translation[0] = translation[0].add(new Vec3D(0, -moveBy[0], 0)));
                runIfPressed.accept(KeyEvent.VK_L, () -> translation[0] = translation[0].add(new Vec3D(0, moveBy[0], 0)));
                runIfPressed.accept(KeyEvent.VK_O, () -> translation[0] = translation[0].add(new Vec3D(0, 0, moveBy[0])));
                runIfPressed.accept(KeyEvent.VK_P, () -> translation[0] = translation[0].add(new Vec3D(0, 0, -moveBy[0])));
                resultingMat[0] = resultingMat[0].mul(new Mat4Transl(translation[0]));
                Solid selectedS = scene.getSolids().get(selectedSolid[0]);
                if (selectedS.getClass().equals(Light.class)) ((Light) selectedS).translate(translation[0]);

                scene.getModelMats().set(selectedSolid[0], resultingMat[0]);
            }

            // rotate camera with arrows
            runIfPressed.accept(KeyEvent.VK_UP, () -> camera[0] = camera[0].addZenith(oneDegree));
            runIfPressed.accept(KeyEvent.VK_DOWN, () -> camera[0] = camera[0].addZenith(-oneDegree));
            runIfPressed.accept(KeyEvent.VK_LEFT, () -> camera[0] = camera[0].addAzimuth(oneDegree));
            runIfPressed.accept(KeyEvent.VK_RIGHT, () -> camera[0] = camera[0].addAzimuth(-oneDegree));
            // rotate camera with mouse "joystick"
            if (Arrays.stream(mouseLook).allMatch(Objects::nonNull)) {
                double xvec = mouseLook[1].getX() - mouseLook[0].getX();
                double yvec = mouseLook[1].getY() - mouseLook[0].getY();
                xvec /= 800;
                yvec /= 800;

                camera[0] = camera[0].addAzimuth(oneDegree * -xvec);
                camera[0] = camera[0].addZenith(oneDegree * -yvec);
            }

            final double[] moveBy = {0.1};
            // zoom camera with mouse wheel
            if (mouseWheelMoved[0] != 0) {
                camera[0] = camera[0].move(camera[0].getViewVector().mul(moveBy[0] * 5 * -mouseWheelMoved[0]));
                mouseWheelMoved[0] = 0;
            }
            // move camera with WSAD
            runIfPressed.accept(KeyEvent.VK_SHIFT, () -> moveBy[0] *= 3);
            runIfPressed.accept(KeyEvent.VK_SPACE, () -> camera[0] = camera[0].move(new Vec3D(0, 0, moveBy[0])));
            runIfPressed.accept(KeyEvent.VK_CONTROL, () -> camera[0] = camera[0].move(new Vec3D(0, 0, -moveBy[0])));
            Vec2D viewVectorXYVec2D = camera[0].getViewVector().ignoreZ().normalized().orElse(new Vec2D(-1));
            Vec3D viewVectorXY = new Vec3D(viewVectorXYVec2D.getX(), viewVectorXYVec2D.getY(), 0);
            runIfPressed.accept(KeyEvent.VK_W, () -> camera[0] = camera[0].move(viewVectorXY.mul(new Vec3D(moveBy[0], moveBy[0], 0))));
            runIfPressed.accept(KeyEvent.VK_S, () -> camera[0] = camera[0].move(viewVectorXY.mul(new Vec3D(-moveBy[0], -moveBy[0], 0))));
            runIfPressed.accept(KeyEvent.VK_A, () -> camera[0] = camera[0].move(new Vec3D(-viewVectorXY.getY(), viewVectorXY.getX(), 0).mul(moveBy[0])));
            runIfPressed.accept(KeyEvent.VK_D, () -> camera[0] = camera[0].move(new Vec3D(viewVectorXY.getY(), -viewVectorXY.getX(), 0).mul(moveBy[0])));

            double k = zBuffer.getHeight() / (double) zBuffer.getWidth();
            Mat4 projectionMatPersp = new Mat4PerspRH(Math.PI / 2, k, 0.1, 200);
            Mat4 projectionMatOrth = new Mat4OrthoRH(16, 12, 0.1, 200);
            renderer.drawScene(
                    scene,
                    camera[0].getViewMatrix(),
                    useOrth[0] ? projectionMatOrth : projectionMatPersp,
                    wireframe[0]
            );
            img.present(panel.getGraphics());
        };

        Thread renderThread = new Thread(() -> {
            Instant last = Instant.ofEpochMilli(0);
            while (true) {
                render.run();
                try {
                    Instant now = Instant.now();
                    Thread.sleep(Math.max(16 - now.toEpochMilli() - last.toEpochMilli(), 0));
                    last = now;
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        renderThread.start();
    }


    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> new Renderer(800, 600).run());
    }
}
