package objectops;

import objectdata.Light;
import objectdata.Vertex;
import transforms.Camera;
import transforms.Col;
import transforms.Vec3D;

import java.util.function.Function;
import java.util.function.Supplier;

public class ShaderUtils {
    public static Vec3D reflect(Vec3D incidentVec, Vec3D normal) {
        double tmp = 2.0 * incidentVec.dot(normal);
        return new Vec3D(incidentVec.getX() - tmp, incidentVec.getY() - tmp, incidentVec.getZ() - tmp).mul(normal);
    }

    public static Function<Vertex, Col> phongShading(Light light, double ambientLightPower, double specularLightPower, double shininess, Function<Vertex, Col> materialColorGen, Supplier<Camera> cameraGen) {
        return vertex -> {
            Col materialColor = materialColorGen.apply(vertex);
            return phongShading(light.getPosition(), light.getColor().mul(materialColor), light.getPower(), materialColor, ambientLightPower, light.getColor(), specularLightPower, shininess, cameraGen.get().getPosition()).apply(vertex);
        };
    }

    public static Function<Vertex, Col> phongShading(Vec3D lightPos, Col diffuseColor, double diffuseLightPower, Col ambientColor, double ambientLightPower, Col specularColor, double specularLightPower, double shininess, Vec3D cameraPos) {
        return vertex -> {
            Vec3D vertexNormal = vertex.getNormal().normalized().orElse(new Vec3D());
            Vec3D modelToLight = lightPos.sub(new Vec3D(vertex.getModelPosition()));
            Vec3D modelToLightNormal = modelToLight.normalized().orElse(new Vec3D());

            // Lambert's cosine law
            double lambertian = Math.min(Math.max(vertexNormal.dot(modelToLightNormal), 0), 1);
            Vec3D reflectedLightVec = reflect(modelToLightNormal.opposite(), vertexNormal);
            Vec3D vecToCamera = cameraPos.sub(new Vec3D(vertex.getModelPosition())).normalized().orElse(new Vec3D());
            // Compute the specular term
            double specAngle = Math.min(Math.max(reflectedLightVec.dot(vecToCamera), 0), 1);
            double specular = Math.pow(specAngle, shininess);
            /*Ka * ambientColor +
                    Kd * lambertian * diffuseColor +
                    Ks * specular * specularColor;*/
            return ambientColor.mul(ambientLightPower).add(
                    diffuseColor.mul(diffuseLightPower).mul(lambertian).mul(1 / Math.pow(modelToLight.length(), 2))
            ).add(
                    specularColor.mul(specularLightPower).mul(specular).mul(1 / Math.pow(modelToLight.length(), 2))
            ).saturate();
        };
    }
}
