package objectops;

import objectdata.Part;
import objectdata.Vertex;
import transforms.Vec3D;

import java.util.List;
import java.util.stream.IntStream;

public class NormalsCalculator {
    private static void calculateNormal(Vertex a, Vertex b, Vertex c, boolean opposite) {
        Vec3D edge1 = new Vec3D(b.getPosition()).sub(new Vec3D(a.getPosition()));
        Vec3D edge2 = new Vec3D(c.getPosition()).sub(new Vec3D(a.getPosition()));
        Vec3D normal = edge1.cross(edge2).normalized().orElse(new Vec3D());
        if (opposite) normal = normal.opposite();
        a.addNormal(normal);
        b.addNormal(normal);
        c.addNormal(normal);
    }

    public static void calculateNormals(List<Vertex> vertices, List<Integer> indices, Part part, boolean opposite) {
        switch (part.getTopology()) {
            case TRIANGLE_FAN:
                Vertex center = vertices.get(indices.get(part.getOffset()));
                IntStream.iterate(part.getOffset() + 1, i -> i + 1).limit(part.getCount() - 2).parallel().forEach(i -> {
                    Vertex current = vertices.get(indices.get(i));
                    Vertex next = vertices.get(indices.get(i + 1));

                    calculateNormal(center, next, current, opposite);
                });
                break;
            case TRIANGLE_STRIP:
                IntStream.iterate(part.getOffset(), i -> i + 1).limit(part.getCount() - 2).parallel().forEach(i -> {
                    Vertex a = vertices.get(indices.get(i));
                    Vertex b = vertices.get(indices.get(i + 1));
                    Vertex c = vertices.get(indices.get(i + 2));

                    calculateNormal(a, b, c, (i % 2 == 0) == opposite);
                });
                break;
        }
    }
}
