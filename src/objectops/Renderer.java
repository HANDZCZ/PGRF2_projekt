package objectops;

import linalg.Lerp;
import objectdata.Part;
import objectdata.Scene;
import objectdata.Solid;
import objectdata.Vertex;
import rasterdata.ZBuffer;
import rasterops.Liner;
import rasterops.Triangler;
import rasterops.TrianglerZBuffer;
import transforms.Col;
import transforms.Mat4;

import java.util.Comparator;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Renderer {
    private final ZBuffer zBuffer;
    private final Liner liner;
    private final Triangler triangler;
    private final Lerp lerp = new Lerp();

    public Renderer(ZBuffer zBuffer) {
        this.zBuffer = zBuffer;
        this.liner = new Liner(zBuffer);
        this.triangler = new TrianglerZBuffer(zBuffer);
    }

    public void drawScene(Scene scene, Mat4 viewMat, Mat4 projectionMat, boolean wireframe) {
        List<Solid> solids = scene.getSolids();
        List<Mat4> modelMats = scene.getModelMats();
        // (M * V) * P = M * (V * P)
        Mat4 viewProjection = viewMat.mul(projectionMat);
        // foreach solid
        IntStream.range(0, solids.size()).parallel().forEach(i -> {
            // create trans matrix (modelMat * viewMat * projMat)
            Mat4 transMat = modelMats.get(i).mul(viewProjection);
            // render solid
            drawSolid(solids.get(i), transMat, modelMats.get(i), wireframe);
        });
    }

    private void drawSolid(Solid solid, Mat4 transMat, Mat4 modelMat, boolean wireframe) {
        // foreach vertex
        List<Vertex> vertices = solid.getVertices().parallelStream()
                // multiply by transMat
                .map(vertex -> vertex.transform(transMat, modelMat))
                .collect(Collectors.toList());

        List<Integer> indices = solid.getIndices();
        List<Part> parts = solid.getParts();

        parts.parallelStream().forEach(part -> {
            switch (part.getTopology()) {
                case LINE_LIST:
                    IntStream.iterate(part.getOffset(), i -> i + 2).limit(part.getCount() / 2).parallel().forEach(i -> {
                        Vertex start = vertices.get(indices.get(i));
                        Vertex end = vertices.get(indices.get(i + 1));

                        if (isOutOfViewSpace(List.of(start, end))) return;

                        List<Vertex> clippedZ = clipZ(start, end)
                                .stream()
                                .map(Vertex::dehomog)
                                .map(vertex -> vertex.toViewport(zBuffer.getWidth(), zBuffer.getHeight()))
                                .collect(Collectors.toList());
                        liner.draw(clippedZ.get(0), clippedZ.get(1), part.getShader().orElse(Vertex::getColor));
                    });
                    break;
                case TRIANGLE_FAN:
                    Vertex center = vertices.get(indices.get(part.getOffset()));
                    IntStream.iterate(part.getOffset() + 1, i -> i + 1).limit(part.getCount() - 2).parallel().forEach(i -> {
                        Vertex current = vertices.get(indices.get(i));
                        Vertex next = vertices.get(indices.get(i + 1));

                        drawTriangle(center, next, current, wireframe, part);
                    });
                    break;
                case TRIANGLE_STRIP:
                    IntStream.iterate(part.getOffset(), i -> i + 1).limit(part.getCount() - 2).parallel().forEach(i -> {
                        Vertex a = vertices.get(indices.get(i));
                        Vertex b = vertices.get(indices.get(i + 1));
                        Vertex c = vertices.get(indices.get(i + 2));

                        drawTriangle(a, b, c, wireframe, part);
                    });
                    break;
            }
        });
    }

    private void drawTriangle(Vertex a, Vertex b, Vertex c, boolean wireframe, Part part) {
        if (isOutOfViewSpace(List.of(a, b, c)))
            return;

        List<Vertex> clippedZ = clipZ(a, b, c)
                .stream()
                .map(Vertex::dehomog)
                .map(vertex -> vertex.toViewport(zBuffer.getWidth(), zBuffer.getHeight()))
                .collect(Collectors.toList());
        Function<Vertex, Col> shader = part.getShader().orElse(Vertex::getColor);
        for (int j = 0; j < clippedZ.size(); j += 3) {
            if (wireframe) {
                liner.draw(clippedZ.get(j), clippedZ.get(j + 1), shader);
                liner.draw(clippedZ.get(j), clippedZ.get(j + 2), shader);
                liner.draw(clippedZ.get(j + 1), clippedZ.get(j + 2), shader);
            } else
                triangler.draw(clippedZ.get(j), clippedZ.get(j + 1), clippedZ.get(j + 2), shader);
        }
    }

    private boolean isOutOfViewSpace(List<Vertex> vertices) {
        boolean allTooLeft = vertices.stream().allMatch(v -> v.getPosition().getX() < -v.getPosition().getW());
        boolean allTooRight = vertices.stream().allMatch(v -> v.getPosition().getX() > v.getPosition().getW());
        boolean allTooUp = vertices.stream().allMatch(v -> v.getPosition().getY() < -v.getPosition().getW());
        boolean allTooDown = vertices.stream().allMatch(v -> v.getPosition().getY() > v.getPosition().getW());
        boolean allTooClose = vertices.stream().allMatch(v -> v.getPosition().getZ() < 0);
        boolean allTooFar = vertices.stream().allMatch(v -> v.getPosition().getZ() > v.getPosition().getW());

        return allTooLeft || allTooRight || allTooUp || allTooDown || allTooClose || allTooFar;
    }

    private List<Vertex> clipZ(Vertex v1, Vertex v2) {
        final Vertex min = (v1.getPosition().getZ() < v2.getPosition().getZ()) ? v1 : v2;
        final Vertex max = (min == v1) ? v2 : v1;
        if (min.getPosition().getZ() < 0) {
            final double t = -min.getPosition().getZ() / (max.getPosition().getZ() - min.getPosition().getZ());
            final Vertex P = lerp.compute(min, max, t);
            return List.of(P, max);
        }
        return List.of(v1, v2);
    }

    private List<Vertex> clipZ(Vertex v1, Vertex v2, Vertex v3) {
        List<Vertex> sorted = List.of(v1, v2, v3)
                .stream()
                .sorted(Comparator.comparingDouble(value -> value.getPosition().getZ()))
                .collect(Collectors.toList());

        switch ((int) sorted.stream().filter(vertex -> vertex.getPosition().getZ() < 0).count()) {
            case 1:
                Vertex out = sorted.get(0);
                double t1 = -out.getPosition().getZ() / (sorted.get(1).getPosition().getZ() - out.getPosition().getZ());
                double t2 = -out.getPosition().getZ() / (sorted.get(2).getPosition().getZ() - out.getPosition().getZ());
                Vertex v1R = lerp.compute(out, sorted.get(1), t1);
                Vertex v2R = lerp.compute(out, sorted.get(2), t2);
                return List.of(v1R, sorted.get(1), sorted.get(2), v1R, v2R, sorted.get(2));
            case 2:
                Vertex in = sorted.get(2);
                t1 = -sorted.get(0).getPosition().getZ() / (in.getPosition().getZ() - sorted.get(0).getPosition().getZ());
                t2 = -sorted.get(1).getPosition().getZ() / (in.getPosition().getZ() - sorted.get(1).getPosition().getZ());
                v1R = lerp.compute(sorted.get(0), in, t1);
                v2R = lerp.compute(sorted.get(1), in, t2);
                return List.of(v1R, v2R, sorted.get(2));
        }
        return List.of(v1, v2, v3);
    }

}
