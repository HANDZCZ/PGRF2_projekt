package linalg;

public class Lerp {
    public <T extends Vectorizable<T>> T compute(T first, T second, double t) {
        return first.mul(1 - t).add(second.mul(t));
    }
}
