package linalg;

public interface Vectorizable<T> {
    T mul(double t);
    T add(T other);
}
