package rasterdata;

import transforms.Col;

import java.util.Optional;

public class ZBuffer {
    private final Raster<Col> colorRaster;
    private final Raster<Double> depthRaster;

    public ZBuffer(Raster<Col> colorRaster) {
        this.colorRaster = colorRaster;
        this.depthRaster = new DepthRaster(colorRaster.getWidth(), colorRaster.getHeight());
    }

    public boolean setPixel(int x, int y, double z, Col color) {
        Optional<Double> oldZ = depthRaster.getPixel(x, y);
        if (oldZ.isPresent()) {
            double oldZVal = oldZ.get();
            if (oldZVal > z) {
                depthRaster.setPixel(x, y, z);
                return colorRaster.setPixel(x, y, color);
            }
        }
        return false;
    }

    public int getWidth() {
        return colorRaster.getWidth();
    }

    public int getHeight() {
        return colorRaster.getHeight();
    }

    public void clear() {
        colorRaster.clear();
        depthRaster.clear();
    }
}
