package rasterdata;

import transforms.Col;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Optional;

public class ColorRaster implements Raster<Col> {

    private final BufferedImage image;
    private final Col background;

    public ColorRaster(int width, int height, Col background) {
        this.image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        this.background = background;
    }

    @Override
    public int getWidth() {
        return this.image.getWidth();
    }

    @Override
    public int getHeight() {
        return this.image.getHeight();
    }

    @Override
    public boolean setPixel(int x, int y, Col pixel) {
        if (x > this.getWidth() - 1 || x < 0 || y > this.getHeight() - 1 || y < 0) return false;
        this.image.setRGB(x, y, pixel.getRGB());
        return true;
    }

    @Override
    public Optional<Col> getPixel(int x, int y) {
        if (x > this.getWidth() - 1 || x < 0 || y > this.getHeight() - 1 || y < 0) return Optional.empty();
        return Optional.of(new Col(this.image.getRGB(x, y) & 0x00FFFFFF));
    }

    @Override
    public void clear() {
        final Graphics g = this.image.getGraphics();
        if (g == null) return;
        g.setColor(new Color(background.getRGB()));
        g.fillRect(0, 0, getWidth(), getHeight());
    }

    public void present(Graphics g) {
        g.drawImage(image, 0, 0, null);
    }
}
