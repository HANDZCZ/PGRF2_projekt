package rasterdata;

import java.util.Arrays;
import java.util.Optional;

public class DepthRaster implements Raster<Double> {

    private final double[][] raster;

    public DepthRaster(int width, int height) {
        this.raster = new double[width][height];
        clear();
    }

    @Override
    public int getWidth() {
        return this.raster.length;
    }

    @Override
    public int getHeight() {
        return this.raster[0].length;
    }

    @Override
    public boolean setPixel(int x, int y, Double pixel) {
        if (x > this.getWidth() - 1 || x < 0 || y > this.getHeight() - 1 || y < 0) return false;
        this.raster[x][y] = pixel;
        return true;
    }

    @Override
    public Optional<Double> getPixel(int x, int y) {
        if (x > this.getWidth() - 1 || x < 0 || y > this.getHeight() - 1 || y < 0) return Optional.empty();
        return Optional.of(this.raster[x][y]);
    }

    @Override
    public void clear() {
        Arrays.stream(this.raster).forEach(row -> Arrays.fill(row, 1.0));
    }
}
